package ua.danit.controller;


import ua.danit.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.service.AlbumService;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/albums")
public class AlbumController {

  private final AlbumService albumService;

  @Autowired
  public AlbumController(AlbumService AlbumService) {
    this.albumService = AlbumService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<Album> getById(@PathVariable("id") long id)  {
    return albumService.getById(id)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @GetMapping("/{id}/cover")
  public void getCoverById(@PathVariable("id") int id, HttpServletResponse response) throws IOException {
    albumService.getCoverById(id,response);
  }


    /*File ablumFolder = new File("root/albums");
    File find = new File("root/albums/index.html");
    if (ablumFolder.isDirectory()) {
      File[] files = ablumFolder.listFiles();
      if (files[0].getName().equals(find.getName())) {
        System.out.println("Ypppppppppppppppaaaaaaaaaaaaaaaaaa");
      } else {
        System.out.println("noooooooooooooooooooooooooooo");
      }
    }*/

//    File foto = new File();
//    Path path = Paths.get("root/albums/"+id+".jpg");
//    byte[] data = Files.readAllBytes(path);

//    response.getOutputStream().write(data);


  @GetMapping
  public ResponseEntity<List<Album>> getAll() {
    return ResponseEntity.ok(albumService.getAll());
  }

  @PostMapping
  public ResponseEntity<Album> create(@RequestBody Album album) {
    return ResponseEntity.ok(albumService.add(album));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> remove(@PathVariable long id) {
    Optional<Album> found = albumService.remove(id);
    return Optional.ofNullable(found)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());

  }

  @PutMapping
  public ResponseEntity<Album> update(@RequestBody Album album) {
    return ResponseEntity.ok(albumService.update(album));
  }



}
