package ua.danit.controller;


import ua.danit.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.service.UserService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<User> getById(@PathVariable("id") long id) {
    return userService.getById(id)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @GetMapping
  public ResponseEntity<List<User>> getAll() {
    return ResponseEntity.ok(userService.getAll());
  }

  @PostMapping
  public ResponseEntity<User> create(@RequestBody User user) {
    return ResponseEntity.ok(userService.add(user));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> remove(@PathVariable long id) {
    Optional<User> found = userService.remove(id);
    return Optional.ofNullable(found)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());

  }

  @PutMapping
  public ResponseEntity<User> update(@RequestBody User user) {
    return ResponseEntity.ok(userService.update(user));
  }
}

