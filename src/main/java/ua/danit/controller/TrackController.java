package ua.danit.controller;

import ua.danit.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.danit.service.TrackService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tracks")
public class TrackController {

  private final TrackService trackService;

  @Autowired
  public TrackController(TrackService trackService) {
    this.trackService = trackService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<Track> getById(@PathVariable("id") long id) {
    return trackService.getById(id)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());
  }

  @GetMapping
  public ResponseEntity<List<Track>> getAll() {
    return ResponseEntity.ok(trackService.getAll());
  }

  @GetMapping("/{id}/play")
  public void getTrackById(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
    trackService.getTrackById(id, response);
  }

  @PostMapping
  public ResponseEntity<Track> create(@RequestBody Track track) {
    return ResponseEntity.ok(trackService.add(track));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> remove(@PathVariable("id") long id) {
    Optional<Track> found = trackService.remove(id);
    return Optional.ofNullable(found)
        .map(u -> new ResponseEntity<>(u, HttpStatus.OK))
        .orElseGet(() -> ResponseEntity.notFound().build());

  }

  @PutMapping
  public ResponseEntity<Track> update(@RequestBody Track track) {
    return ResponseEntity.ok(trackService.update(track));
  }
}

