package ua.danit.model;


import javax.persistence.*;

@Entity
@Table(name = "MUSICBOX_ALBUMS")
public class Album {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "ALBUM_NAME")
  private String name;

  public Album(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  public Album() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
