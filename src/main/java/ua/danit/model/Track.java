package ua.danit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MUSICBOX_TRACKS")
public class Track {

  @Id
  @Column(name = "ID")
  private Long id;

  @Column(name = "TRACK_NAME")
  private String name;

  @Column(name = "ALBUM_ID")
  private Long albumId;

  public Track(Long id, String name, Long albumId) {
    this.id = id;
    this.name = name;
    this.albumId = albumId;
  }

  public Track() {
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getAlbumId() {
    return albumId;
  }

  public void setAlbumId(Long albumId) {
    this.albumId = albumId;
  }


}
