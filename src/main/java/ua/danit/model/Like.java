package ua.danit.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MUSICBOX_LIKES")
public class Like {

  @EmbeddedId
  private LikePK likePK;

  @Embeddable
  public static class LikePK implements Serializable {

    @Column(name = "TRACK_ID")
    private Long trackId;

    @Column(name = "USER_ID")
    private Long userId;

    public LikePK(Long trackId, Long userId) {
      this.trackId = trackId;
      this.userId = userId;
    }

    public LikePK() {
    }

    public Long getTrackId() {
      return trackId;
    }

    public void setTrackId(Long trackId) {
      this.trackId = trackId;
    }

    public Long getUserId() {
      return userId;
    }

    public void setUserId(Long userId) {
      this.userId = userId;
    }
  }
}
