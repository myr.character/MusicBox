package ua.danit.service;

import ua.danit.dao.UserDao;
import ua.danit.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

  @Autowired
  private UserDao userDao;

  public Optional<User> getById(long id) {
    return userDao.getById(id);
  }
  public List getAll() {
    return userDao.getAll();
  }

  public User add(User user) {
    return userDao.add(user);
  }

  public User update (User user) {
    return userDao.update(user);
  }

  public Optional<User> remove(long id)  {
    return userDao.remove(id);
  }

}
