package ua.danit.service;

import ua.danit.dao.TrackDao;
import ua.danit.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
public class TrackService {

  @Autowired
  private TrackDao trackDao;

  public Optional<Track> getById(long id) {
    return trackDao.getById(id);
  }

  public List getAll() {
    return trackDao.getAll();
  }

  public Track add(Track Track) {
    return trackDao.add(Track);
  }

  public Track update(Track Track) {
    return trackDao.update(Track);
  }

  public Optional<Track> remove(long id) {
    File tracksDirecoty = new File("root/tracks");

    for (File fileDelete : tracksDirecoty.listFiles()) {
      int lastMark = fileDelete.getName().indexOf(".");
      String fileName = fileDelete.getName().substring(0, lastMark);

      if (fileName.equals("" + id)) {
        fileDelete.delete();
      }
    }
    return trackDao.remove(id);
  }

  public void getTrackById(Long id, HttpServletResponse response) throws IOException {
    File tracksDirectory = new File("root/tracks");
    Path path = null;


    for (File file : tracksDirectory.listFiles()) {
      int lastMark = file.getName().indexOf(".");
      String name = file.getName().substring(0, lastMark);

      if (name.equals("" + id)) {
        path = Paths.get(file.getAbsolutePath());
        byte[] data = Files.readAllBytes(path);
        response.getOutputStream().write(data);
        return;
      }
    }
  }
}
