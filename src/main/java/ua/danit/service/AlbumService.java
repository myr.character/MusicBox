package ua.danit.service;

import ua.danit.dao.AlbumDao;
import ua.danit.dao.TrackDao;
import ua.danit.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.model.Track;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
public class AlbumService {

  @Autowired
  private  AlbumDao albumDao;

  @Autowired
  private TrackDao trackDao;

  public Optional<Album> getById(long id) {
    return albumDao.getById(id);
  }
  public List getAll() {
    return albumDao.getAll();
  }

  public Album add(Album Album) {
    return albumDao.add(Album);
  }

  public Album update (Album Album) {
    return albumDao.update(Album);
  }

  public Optional<Album> remove(long id) {
    File trackDirectory = new File("root/tracks");
    List<Track> albumsTracks = trackDao.getAllByAlbumId(id);

    for (File filedelete : trackDirectory.listFiles()) {
      int lastMark = filedelete.getName().indexOf(".");

      String name = filedelete.getName().substring(0, lastMark);

    for (Track checkTrack : albumsTracks) {
      if (checkTrack.getId().equals(name)) {
        filedelete.delete();
        break;
      }
    }
    }
    return albumDao.remove(id);
  }

  public void getCoverById(long id, HttpServletResponse response) throws IOException {
    File albumDirectory = new File("root/covers");
    Path path = null;

    for (File file : albumDirectory.listFiles()) {

      int lastMark = file.getName().indexOf(".");

      String name = file.getName().substring(0, lastMark);

      if (name.equals("" + id)) {
        path = Paths.get(file.getAbsolutePath());
        byte[] data = Files.readAllBytes(path);
        response.getOutputStream().write(data);
        return;
      }
    }
  }

}
