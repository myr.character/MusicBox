package ua.danit.dao;

import org.springframework.stereotype.Repository;
import ua.danit.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Repository
public class AlbumDao {


  private final EntityManager manager;

  @Autowired
  public AlbumDao(EntityManager entityManager) {
    this.manager = entityManager;
  }


  @Transactional
  public Optional<Album> getById(long id) {
    Album album = manager.find(Album.class, id);
    return Optional.ofNullable(album);
  }

  public List getAll() {
    return manager.createQuery("from Album").getResultList();
  }


  @Transactional
  public Album add(Album album) {
    manager.persist(album);
    return album;
  }

  @Transactional
  public Album update (Album album) {
    return manager.merge(album);
  }

  @Transactional
  public Optional<Album> remove(long id) {
    Album found = manager.find(Album.class, id);
    manager.remove(found);
    return Optional.ofNullable(found);
  }

}
