package ua.danit.dao;

import org.springframework.stereotype.Repository;
import ua.danit.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public class UserDao {

  private final EntityManager manager;

  @Autowired
  public UserDao(EntityManager entityManager) {
    this.manager = entityManager;
  }


  @Transactional
  public Optional<User> getById(long id) {
    User user = manager.find(User.class, id);
    return Optional.ofNullable(user);
  }

  public List getAll() {
    return manager.createQuery("from User").getResultList();
  }


  @Transactional
  public User add(User user) {
    manager.persist(user);
    return user;
  }

  @Transactional
  public User update (User user) {
    return manager.merge(user);
  }

  @Transactional
  public Optional<User> remove(long id) {
    User found = manager.find(User.class, id);
    manager.remove(found);
    return Optional.ofNullable(found);
  }
}
