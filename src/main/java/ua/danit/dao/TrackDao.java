package ua.danit.dao;

import org.springframework.stereotype.Repository;
import ua.danit.model.Track;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public class TrackDao {

  private final EntityManager manager;

  @Autowired
  public TrackDao(EntityManager entityManager) {
    this.manager = entityManager;
  }


  @Transactional
  public Optional<Track> getById(long id) {
    Track track = manager.find(Track.class, id);
    return Optional.ofNullable(track);
  }

  public List getAll() {
    return manager.createQuery("from Track").getResultList();
  }


  @Transactional
  public Track add(Track track) {
    manager.persist(track);
    return track;
  }

  @Transactional
  public Track update (Track track) {
    return manager.merge(track);
  }

  @Transactional
  public Optional<Track> remove(long id) {
    Track found = manager.find(Track.class, id);
    manager.remove(found);
    return Optional.ofNullable(found);
  }

  @Transactional
  public List<Track> getAllByAlbumId(long id) {
    return manager.createQuery(String.format("from Track where albumId = %d", id)).getResultList();
  }
}
